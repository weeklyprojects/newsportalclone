//
//  ContentView.swift
//  NewsPortalClone
//
//  Created by F1 Soft on 09/05/2021.
//

import SwiftUI

struct ContentView: View {
    @State var leftMenuShow: Bool = false
    var body: some View {
        GeometryReader { geo in
            NavigationView {
                ZStack(alignment: .leading) {
                    VStack(alignment: .center, spacing: 0, content: {
                        ToolBarView(geo: geo.size, leftMenuShow: $leftMenuShow)
                        UploadedDateView()
                        NewsList()
                    })
                    .offset(x: self.leftMenuShow ? geo.size.width / 1.4 : 0)
            
                    LeftNavigationDrawerView(geo: geo.size)
                        .transition(.move(edge: .leading))
                        .foregroundColor(.black)
                        .offset(x: self.leftMenuShow ? 0 : -geo.size.width / 1.4)
                }
            }
        }
    }
}

struct UploadedDateView: View {
    var body: some View {
        VStack(alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/, spacing: 10, content: {
            HStack {
                Text("Uploaded at:")
                    .font(.subheadline)
                    .fontWeight(.light)
                    .foregroundColor(.black)
                Text("May 9th, Sunday ")
                    .font(.subheadline)
                    .fontWeight(.semibold)
                    .foregroundColor(.black)
            }
            .padding(.bottom, 10)
        })
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
