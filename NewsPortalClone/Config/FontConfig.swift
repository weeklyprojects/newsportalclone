import Foundation
import SwiftUI

extension Font {
    static func boldFont(ofSize size: CGFloat) -> Font {
        return Font.custom("PlayfairDisplay-Bold", size: size)
    }
    
    static func regularFont(ofSize size: CGFloat) -> Font {
        return Font.custom("PlayfairDisplay-Regular", size: size)
    }
}
