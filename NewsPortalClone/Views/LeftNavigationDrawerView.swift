//
//  LeftNavigationDrawerView.swift
//  NewsPortalClone
//
//  Created by shreejwal giri on 12/05/2021.
//

import SwiftUI

struct MenuList: Identifiable {
    let id = UUID()
    let icon: String
    let title: String
}
struct LeftNavigationDrawerView: View {
    @State var geo: CGSize = CGSize(width: 0, height: 0)
    
    var menuListMain: [MenuList] = [
        MenuList(icon: "network", title: "Top Stories"),
        MenuList(icon: "network", title: "Most Popular"),
        MenuList(icon: "network", title: "Saved For Later")
    ]
    
    var menuListOthers: [MenuList] = [
        MenuList(icon: "book.circle.fill", title: "Recently Viewed"),
        MenuList(icon: "book.circle.fill", title: "Test"),
        MenuList(icon: "book.circle.fill", title: "Test Two")
    ]
    
    var body: some View {
        List {
            Section(header: Text("Top Section".uppercased())) {
                ForEach (menuListMain) { (menu) in
                    Button(action: {
                    }, label: {
                        HStack(alignment: .center, spacing: 10, content: {
                            Image(systemName: menu.icon)
                            Text(menu.title)
                        })
                    })
                }
            }
            
            Section(header: Text("Others Section".uppercased())) {
                ForEach (menuListOthers) { (menu) in
                    Button(action: {}, label: {
                        HStack(alignment: .center, spacing: 10, content: {
                            Image(systemName: menu.icon)
                            Text(menu.title)
                        })
                    })
                }
            }
        }
//        .frame(maxWidth: .infinity, alignment: .leading)
        .frame(width: geo.width/1.4, alignment: .leading)
    }
   
}

struct LeftNavigationDrawerView_Previews: PreviewProvider {
    static var previews: some View {
        LeftNavigationDrawerView()
    }
}
