//
//  NavigateView.swift
//  NewsPortalClone
//
//  Created by shreejwal giri on 15/05/2021.
//

import SwiftUI

struct NavigateView: View {
    var body: some View {
        NavigationView {
            VStack {
                Text("Hello")
                
            }.navigationBarTitle("2nd View")
        }
    }
}

struct NavigateView_Previews: PreviewProvider {
    static var previews: some View {
        NavigateView()
    }
}
