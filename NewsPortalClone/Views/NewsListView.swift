//
//  NewsListView.swift
//  NewsPortalClone
//
//  Created by shreejwal giri on 10/05/2021.
//

import SwiftUI

struct NewsContent: Identifiable {
    var id = UUID()
    let title: String
    let description: String
    let coverImage: String
    let uploadedTime: String
}

struct NewsList: View {
    var newsContent = [
        NewsContent(
            title: "Titles are a strong differentiating factor between fake and real news.",
            description: "By far the biggest difference between fake and real news sources is the title. Specifically, we find, across both the Buzzfeed data set and ours, that fake news titles are longer than real news titles and contain simpler words in ",
            coverImage: "imgOne",
            uploadedTime: "2h Ago"
        ),
        NewsContent(
            title: "have you set the right font names (casesensetive) in Info.plist and the right key Fonts provided by application?",
            description: "Thanks for your response. The only difference between what you have suggested and what I have already done is that you put you fonts in a folder, where I put mine in a folder. Do you think that makes a difference?",
            coverImage: "imgTwo",
            uploadedTime: "1h Ago"
        ),
        NewsContent(
            title: "This is the title of the text",
            description: "And this is the content of the text, and it has to be a paragragh to have the full graps of the contect ot be shown",
            coverImage: "imgOne",
            uploadedTime: "20m Ago"
        ),
        NewsContent(
            title: "This is the title of the text",
            description: "And this is the content of the text, and it has to be a paragragh to have the full graps of the contect ot be shown ",
            coverImage: "imgTwo",
            uploadedTime: "5m Ago"
        )
    ]
    
    var body: some View {
        ScrollView(/*@START_MENU_TOKEN@*/.vertical/*@END_MENU_TOKEN@*/, showsIndicators: false, content: {
            ForEach (newsContent) { (news) in
                Button(action: {
                }, label: {
                    VStack(alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/, spacing: 0, content: {
                        NewsCell(title: news.title, description: news.description, coverImage: news.coverImage, uploadedTime: news.uploadedTime)
                            .listRowInsets(EdgeInsets())
                    })
                })
                .foregroundColor(.black)
               
            }
        })
    }
}

struct NewsCell: View {
    
    @State var title: String = ""
    @State var description: String = ""
    @State var coverImage: String = ""
    @State var uploadedTime: String = ""
    
    var body: some View {
        VStack {
            Image(coverImage)
                .resizable()
                .aspectRatio(contentMode: /*@START_MENU_TOKEN@*/.fill/*@END_MENU_TOKEN@*/)
            
            VStack(alignment: .leading, spacing: 8, content: {
                Text(title)
                    .font(.boldFont(ofSize: 20))
                Text(description)
                    .font(.regularFont(ofSize: 14))
                    .foregroundColor(.gray)
                    .lineLimit(5)
            })
            .padding([.leading, .trailing], 10)
           Spacer()
        }
        .frame(width: UIScreen.main.bounds.size.width, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
    }
}

struct NewsList_Previews: PreviewProvider {
    static var previews: some View {
        NewsList()
    }
}
