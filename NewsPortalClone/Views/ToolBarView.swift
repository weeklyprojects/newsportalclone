//
//  ToolBarView.swift
//  NewsPortalClone
//
//  Created by shreejwal giri on 10/05/2021.
//

import SwiftUI

struct ToolBarView: View {
    @State var geo: CGSize = CGSize(width: 0, height: 0)
    @Binding var leftMenuShow: Bool
    var body: some View {
        VStack {
            Text("")
        }
        .navigationBarTitle(Text(""), displayMode: .inline)
            .toolbar(content: {

                ToolbarItem(placement: .navigationBarLeading) {
                    Button(action: {
                        withAnimation {
                            leftMenuShow.toggle()
                        }
                       
                    }, label: {Label("", systemImage: "line.horizontal.3")})
                }

                ToolbarItem(placement: .principal) {
                    Image("brand-logo")
                        .resizable()
                        .aspectRatio(contentMode: .fit)
                        .frame(width: geo.width / 1.6)

                }

                ToolbarItem(placement: .navigationBarTrailing) {
                    Button(action: {}, label: {Label("", systemImage: "bell")})
                }
            })
    }
}

struct ToolBarView_Previews: PreviewProvider {
    static var previews: some View {
        ToolBarView( leftMenuShow: .constant(true))
    }
}
