//
//  NewsPortalCloneApp.swift
//  NewsPortalClone
//
//  Created by F1 Soft on 09/05/2021.
//

import SwiftUI

@main
struct NewsPortalCloneApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
